package es.enrrodech.orangebank.model

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
data class Transaction(
        @SerializedName("id") val transactionId: Int,
        @SerializedName("date") val date: Date?,
        @SerializedName("amount") val amount: Double,
        @SerializedName("fee") val fee: Double,
        @SerializedName("description") val description: String?) {

    fun getTotal(): Double = amount + fee

    fun getTotalWithCurrency() : String = getTotal().toString() + "€"

    fun getDateAsString(): String {
        return if (date != null) {
            val format = SimpleDateFormat("EEEE, dd MMMM yyyy", Locale.getDefault())
            format.format(date)
        } else
            ""
    }
}