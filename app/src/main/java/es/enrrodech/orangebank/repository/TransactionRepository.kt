package es.enrrodech.orangebank.repository

import android.arch.lifecycle.MutableLiveData
import es.enrrodech.orangebank.core.OrangeBankAPI
import es.enrrodech.orangebank.model.Transaction
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
class TransactionRepository {

    //  TODO: Crear Resource model para guardar el estado en el que está el dato (SUCCESS, LOADING/UPDATING, ERROR)
    val transactionObservable: MutableLiveData<List<Transaction>> = MutableLiveData()

    /**
     *
     * Obtiene las transacciones a través del web service
     *
     */
    fun getTransactionsFromWS() {
        //  TODO: Indicar que el dato se está actualizando
        OrangeBankAPI.getTransactions().enqueue(object : Callback<List<Transaction>> {

            override fun onResponse(call: Call<List<Transaction>>?, response: Response<List<Transaction>>?) {
                if (response != null && response.isSuccessful) {
                    transactionObservable.postValue(sortAndFilterTransactionList(response.body()))
                } else {
                    transactionObservable.postValue(null)
                }
            }

            override fun onFailure(call: Call<List<Transaction>>?, t: Throwable?) {
                transactionObservable.postValue(null)
            }
        })
    }

    /**
     *
     * Ordena por fecha (de mas reciente a mas antigua) y filtra las transacciones para eliminar aquellas sin fecha y a mismo id se quede con la que tenga la fecha mas reciente
     *
     */
    private fun sortAndFilterTransactionList(transactionList: List<Transaction>?): List<Transaction> {
        val transactionMutableList: MutableList<Transaction> = transactionList?.toMutableList()!!
        transactionMutableList.addAll(transactionList)
        transactionMutableList.removeAll { it.date == null }
        transactionMutableList.sortByDescending { it.date }
        return transactionMutableList.distinctBy { it.transactionId }
    }

}