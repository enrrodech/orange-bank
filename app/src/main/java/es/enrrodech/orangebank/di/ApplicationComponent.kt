package es.enrrodech.orangebank.di

import android.content.Context
import dagger.Component
import es.enrrodech.orangebank.OrangeBankApp
import es.enrrodech.orangebank.repository.TransactionRepository
import es.enrrodech.orangebank.ui.transactionslist.TransactionListViewModel
import javax.inject.Singleton

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    fun inject(transactionListViewModel: TransactionListViewModel)

    fun getApplication(): OrangeBankApp

    fun getContext(): Context

    fun getTransactionRepository(): TransactionRepository
}