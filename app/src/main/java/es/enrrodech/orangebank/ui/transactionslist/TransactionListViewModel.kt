package es.enrrodech.orangebank.ui.transactionslist

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import es.enrrodech.orangebank.OrangeBankApp
import es.enrrodech.orangebank.model.Transaction
import es.enrrodech.orangebank.repository.TransactionRepository
import javax.inject.Inject

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
class TransactionListViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var transactionRepository: TransactionRepository

    init {
        (application as? OrangeBankApp)?.component?.inject(this)
        transactionRepository.getTransactionsFromWS()
    }

    fun getTransactions(): MutableLiveData<List<Transaction>> {
        return transactionRepository.transactionObservable
    }

}