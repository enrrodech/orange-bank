package es.enrrodech.orangebank.ui.transactionslist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.CardView
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.ListView
import android.widget.ProgressBar
import android.widget.TextView
import es.enrrodech.orangebank.R
import es.enrrodech.orangebank.model.Transaction

class MainActivity : AppCompatActivity() {

    private val transactions: MutableList<Transaction> = mutableListOf()
    private lateinit var loading: ProgressBar
    private lateinit var lastTransactionCard: CardView
    private lateinit var lastTransactionDateTextView: TextView
    private lateinit var lastTransactionDescriptionTextView: TextView
    private lateinit var lastTransactionTotalTextView: TextView
    private lateinit var iconImageView: ImageView
    private lateinit var transactionsListView: ListView
    private lateinit var transitionAdapter: TransactionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        val viewModel = ViewModelProviders.of(this).get(TransactionListViewModel::class.java)

        loading = findViewById(R.id.main_loading)
        lastTransactionCard = findViewById(R.id.main_last_transaction_card)
        lastTransactionCard.visibility = View.GONE
        lastTransactionDateTextView = findViewById(R.id.main_last_transaction_date_textview)
        lastTransactionDescriptionTextView = findViewById(R.id.main_last_transaction_description_textview)
        lastTransactionTotalTextView = findViewById(R.id.main_last_transaction_total_textview)
        iconImageView = findViewById(R.id.main_last_transaction_icon)

        transactionsListView = findViewById(R.id.main_transaction_listview)
        transactionsListView.visibility = View.GONE
        transitionAdapter = TransactionAdapter(this, transactions)
        transactionsListView.adapter = transitionAdapter

        viewModel.getTransactions().observe(this, Observer {
            //  TODO: Manejar los posibles errores (sin internet, error de servidor, ...)
            if (it != null && !it.isEmpty()) {
                updateUI(it)
            }
        })
    }

    private fun updateUI(transactionListResponse: List<Transaction>) {
        val transaction: Transaction = transactionListResponse[0]
        lastTransactionDateTextView.text = transaction.getDateAsString()
        lastTransactionDescriptionTextView.text = if (!TextUtils.isEmpty(transaction.description)) transaction.description else getString(R.string.no_description)
        lastTransactionTotalTextView.text = transaction.getTotalWithCurrency()
        iconImageView.setBackgroundResource(if (transaction.getTotal() < 0) R.drawable.ic_arrow_downward else R.drawable.ic_arrow_upward)

        loading.visibility = View.GONE
        lastTransactionCard.visibility = View.VISIBLE
        transactionsListView.visibility = View.VISIBLE

        refreshList(transactionListResponse)
    }

    private fun refreshList(transactionListResponse: List<Transaction>) {
        transactions.addAll(transactionListResponse.subList(1, transactionListResponse.size))
        transitionAdapter.notifyDataSetChanged()
    }

}
