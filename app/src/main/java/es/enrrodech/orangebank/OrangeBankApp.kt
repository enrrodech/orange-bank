package es.enrrodech.orangebank

import android.app.Application
import es.enrrodech.orangebank.di.ApplicationComponent
import es.enrrodech.orangebank.di.ApplicationModule
import es.enrrodech.orangebank.di.DaggerApplicationComponent

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
class OrangeBankApp: Application() {

    val component: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this)).build()
    }

}