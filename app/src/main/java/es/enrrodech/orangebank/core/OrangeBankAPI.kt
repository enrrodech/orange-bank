package es.enrrodech.orangebank.core

import com.google.gson.*
import es.enrrodech.orangebank.model.Transaction
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.lang.reflect.Type
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 * Aquí se incluirá todas las llamadas que se hagan a los Web Services
 */
class OrangeBankAPI {

    companion object {

        fun getTransactions(): Call<List<Transaction>> {
            return createService().getTransactions()
        }

        private fun createService(): OrangeBankServices = Retrofit.Builder()
                .baseUrl("https://api.myjson.com/")
                .addConverterFactory(GsonConverterFactory.create(createGsonDateFormat()))
                .build()
                .create(OrangeBankServices::class.java)

        private fun createGsonDateFormat(): Gson {
            val gBuilder = GsonBuilder().registerTypeAdapter(Date::class.java, DateDeserializer())
            return gBuilder.create()
        }

        private class DateDeserializer: JsonDeserializer<Date> {

            override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Date? {
                return try {
                    SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(json?.asString)
                } catch (e: ParseException) {
                    null
                }
            }
        }

    }

    interface OrangeBankServices {

        @GET("bins/1a30k8")
        fun getTransactions(): Call<List<Transaction>>

    }
}