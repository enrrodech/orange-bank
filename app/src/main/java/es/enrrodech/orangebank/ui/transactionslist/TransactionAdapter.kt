package es.enrrodech.orangebank.ui.transactionslist

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import es.enrrodech.orangebank.R
import es.enrrodech.orangebank.model.Transaction

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
class TransactionAdapter(context: Context, transactionsList: List<Transaction>): ArrayAdapter<Transaction>(context, R.layout.layout_transaction_item, transactionsList) {

    internal class ViewHolder {
        var iconImageView: ImageView? = null
        var dateTextView: TextView? = null
        var descriptionTextView: TextView? = null
        var totalTextView: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val holder: ViewHolder
        val retView: View

        if (convertView == null) {
            retView = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.layout_transaction_item, parent, false)
            holder = ViewHolder()

            holder.iconImageView = retView.findViewById(R.id.transaction_item_icon)
            holder.dateTextView = retView.findViewById(R.id.transaction_item_date_textview)
            holder.descriptionTextView = retView.findViewById(R.id.transaction_item_description_textview)
            holder.totalTextView = retView.findViewById(R.id.transaction_item_total_textview)

            retView.tag = holder

        } else {
            retView = convertView
            holder = retView.tag as ViewHolder
        }
        val transaction: Transaction = getItem(position)
        holder.dateTextView!!.text = transaction.getDateAsString()
        holder.descriptionTextView!!.text = if (!TextUtils.isEmpty(transaction.description)) transaction.description else context.getString(R.string.no_description)
        holder.totalTextView!!.text = transaction.getTotalWithCurrency()
        updateIcon(holder, transaction)

        return retView
    }

    /**
     *
     *  Actualiza
     *
     */
    private fun updateIcon(holder: ViewHolder, transaction: Transaction) {
        holder.iconImageView?.setBackgroundResource(if (transaction.getTotal() < 0) R.drawable.ic_arrow_downward else R.drawable.ic_arrow_upward)
    }

}