package es.enrrodech.orangebank.di

import android.content.Context
import dagger.Module
import dagger.Provides
import es.enrrodech.orangebank.OrangeBankApp
import es.enrrodech.orangebank.repository.TransactionRepository
import javax.inject.Singleton

/**
 * Created by Enrique Rodicio on 5/9/18.
 *
 */
@Module
class ApplicationModule(private var application: OrangeBankApp) {

    @Provides
    @Singleton
    fun provideApplication(): OrangeBankApp = application

    @Provides
    @Singleton
    fun provideContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideTransactionRepository(): TransactionRepository = TransactionRepository()

}